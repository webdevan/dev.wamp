@echo off
cd c:\dev\bin\


echo.
echo MAKE SURE YOU ARE RUNNING THIS AS ADMINISTRATOR!
echo This bat file will help install dev.wamp
echo Please make sure you extracted dev.wamp to c:\dev
echo You will also get some errors if you have run this bat before, but it will probably still work
echo If install.bat appears to hang for more than 30s then maybe there is a windows popup hiding somewhere that requires a click!
echo.
pause


echo.
echo Installing all the Microsoft Visual C++ Reditributable packages that you need (if you already have it then just click repair or cancel or close. If it asks you to restart, dont restart)...
echo vcredist_x86 (2008)
call "c:\dev\install\vcredist_x86 (2008).exe"
echo vcredist_x64 (2008)
call "c:\dev\install\vcredist_x64 (2008).exe"
echo vcredist_x86 (2012)
call "c:\dev\install\vcredist_x86 (2012).exe"
echo vcredist_x64 (2012)
call "c:\dev\install\vcredist_x64 (2012).exe"
echo vc_redist.x86 (2015)
call "c:\dev\install\vc_redist.x86 (2015).exe"
echo vc_redist.x64 (2015)
call "c:\dev\install\vc_redist.x64 (2015).exe"
echo.
pause


echo.
echo Installing Apache system service...
c:\dev\bin\apache\2.4\bin\httpd.exe -k install
echo.
echo done!
echo.
pause


echo.
echo Installing Maria DB system service...
c:\dev\bin\mariadb\10.1\bin\mysqld.exe -k install
c:\dev\bin\mariadb\10.1\bin\mysql_install_db.exe --service="MariaDB10.1" --datadir="C:\dev\bin\mariadb\database_data"
echo.
echo.
echo done!
echo.
pause


echo.
echo Installing Acrylic DNS system service...
cd c:\dev\bin\acrylic
call InstallAcrylicService.bat
cd c:\dev\bin
echo.
echo done!
echo.
pause


echo.
echo Adding PHP 7.0 and Maria DB 10.1 to PATH variable (this can take a while)
setlocal EnableDelayedExpansion
set "pathToInsert="
set "pathOne=C:\dev\bin\mariadb\10.1\bin"
if "!path:%pathOne%=!" equ "%path%" (
	set "pathToInsert=;%pathOne%"
)
set "pathTwo=C:\dev\bin\php\7.0"
if "!path:%pathTwo%=!" equ "%path%" (
	set "pathToInsert=%pathToInsert%;%pathTwo%"
)
setx PATH "%PATH%%pathToInsert%" /m
echo.
echo done
echo.
pause


echo.
echo Creating start-up shortcut to MySQLNotifier (this will let you monitor the services from your system tray)
set SCRIPT="%TEMP%\%RANDOM%-%RANDOM%-%RANDOM%-%RANDOM%.vbs"
echo Set oWS = WScript.CreateObject("WScript.Shell") >> %SCRIPT%
echo sLinkFile = "%USERPROFILE%\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\dev.wamp.lnk" >> %SCRIPT%
echo Set oLink = oWS.CreateShortcut(sLinkFile) >> %SCRIPT%
echo oLink.TargetPath = "c:\dev\bin\mysql notifier\MySQLNotifier.exe" >> %SCRIPT%
echo oLink.Save >> %SCRIPT%
cscript /nologo %SCRIPT%
del %SCRIPT%
echo.
echo done
echo.
pause


echo.
echo Attempting to start up dev.wamp
echo.
echo Starting MySQLNotifier...
start "" "c:\dev\bin\mysql notifier\MySQLNotifier.exe"
echo.
echo Starting "AcrylicServiceController"...
sc start "AcrylicServiceController"
echo.
echo Starting "MariaDB10.1"...
sc start "MariaDB10.1"
echo.
echo Starting "Apache2.4"...
sc start "Apache2.4"
echo.
echo done
echo.
pause


echo.
echo Automatic part complete (hopefully it all worked). Onto the manual part.
echo In order to use Acrylic DNS you need to change the DNS settings on your network adaptor. You will need to change your primary DNS server to 127.0.0.1 (IPv4) and ::1 (IPv6).
echo For secondary DNS server you can use 8.8.8.8 and 2001:4860:4860::8888 (Google DNS)
echo.
pause


echo.
echo "MySQLNotifier should be running in your system tray now. In order to get the most out of it you need to manually add the services you want to monitor by right clicking the icon and going: Actions > Manage Monitored Items > Add > Windows Service, and find Apache2.4, MariaDB10.1, and Acrylic DNS Proxy"
echo.
pause

echo.
echo dev.wamp is not installed! You should be able to navigate to XX.phpinfo.dev where XX is the php version 53/54/55/56/70
echo.
pause